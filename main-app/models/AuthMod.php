<?php

class AuthMod extends CI_Model{

	function __construct() {
		$this->details = array();
		parent::__construct();
        $this->load->helper('security');
    }

	public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $sql = "SELECT id,user_pass,user_email FROM admin WHERE user_name = ? AND is_activated = 1";
        $query = $this->db->query($sql, array($username));

        $get_pass = $query->row();

//        echo $password;
//        echo $get_pass->user_pass;
//
//        return 0;

        if ($get_pass) {
            if (password_verify($password, $get_pass->user_pass)) {
                $data = array(
                    'u_name' => $username,
                );
                $sess_data = array(
                    'user_name' => $username,
                    'logged_in' => TRUE,
                    'email' => $get_pass->user_email,
                    'user_id' => $get_pass->id,
                );
                $this->session->set_userdata($sess_data);

                return $data;

            }
            else {
                $data = array(
                    'error' => 'Username and password did not match. Please try again.',
                );
                return $data;
            }
        }
        else
        {
            $data = array(
                'error' => 'Username and password did not match. Please try again.',
            );
            return $data;
        }

    }

	public function add_new_user()
	{
		$username = $this->input->post('username');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$institute_name = $this->input->post('institute_name');
		$email = $this->input->post('email');
		$default_password = $this->input->post('password');

        $password = password_hash($default_password, PASSWORD_DEFAULT);

        $sqlemail = "SELECT user_name FROM admin WHERE user_email= ?";
        $queryemail = $this->db->query($sqlemail, array($email));

        $check_email = $queryemail->row();

        if($check_email){
            $data = array(
                'error' => 'Email already exists. Please register with different email.',
            );
            return $data;
        }

        else {

            $sql = "SELECT user_name FROM admin WHERE user_name=?";
            $query = $this->db->query($sql, array($username));

            $check_username = $query->row();

            if ($check_username) {
                $data = array(
                    'error' => 'Username already exists. Please select different username and try again.',
                );
                return $data;
            }

            else {

                $this->load->helper('string');
                $verification_code = random_string('alnum', 20);

                $sql2 = "INSERT INTO admin (user_name, user_email, user_pass, first_name, last_name, institute_name, verification_code, profile_pic_path) VALUES (?,?,?,?,?,?,?,'default_avatar.png')";
                $query2 = $this->db->query($sql2, array($username, $email, $password, $first_name, $last_name, $institute_name, $verification_code));

                $add_new_user = $query2;

                if ($add_new_user) {
                    $this->email->from('harekrsna.viral@gmail.com', 'Quiz System');
                    $this->email->to($email);

                    $this->email->subject('Activate Your Account For Quiz System');
                    $this->email->message("Dear User,\nPlease click on below URL or paste into your browser to verify your Email Address\n\n" . base_url() . "verify/" . $verification_code . "\n" . "\n\nThanks\nAdmin Team ");

                    $this->email->send();

                    $data = array(
                        'u_name' => $username,
                        'msg' => 'Welcome ',
                    );

                    return $data;

                } else {
                    $data = array(
                        'error' => 'Something Went Wrong. Please Try Again.',
                    );
                    return $data;
                }
            }
        }
	}

    public function verifyEmailAddress($verificationcode){
        $sql = "UPDATE admin SET is_activated='1' WHERE verification_code = ?";
        $this->db->query($sql, array($verificationcode));
        return $this->db->affected_rows();
    }


}

