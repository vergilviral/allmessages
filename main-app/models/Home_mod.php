<?php

class Home_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		parent::__construct();
	}

	public function get_info()
	{

		$sql1 = "SELECT msg_id FROM contact_form";
		$query1 = $this->db->query($sql1);

		$contact_form_submissions = $query1->num_rows();

        $sql2 = "SELECT req_id FROM signup_requests";
        $query2 = $this->db->query($sql2);

        $signup_requests = $query2->num_rows();

        $sql3 = "SELECT feedback_id FROM feedbacks where source='Admin Panel'";
        $query3 = $this->db->query($sql3);

        $feedbacks_from_admin_panel = $query3->num_rows();


        $sql4 = "SELECT feedback_id FROM feedbacks where source='Quiz System'";
        $query4 = $this->db->query($sql4);

        $feedbacks_from_quiz_system = $query4->num_rows();

        if (isset($contact_form_submissions) && isset($signup_requests) && isset($feedbacks_from_admin_panel) && isset($feedbacks_from_quiz_system)) {
            $data = array(
                'contact' => $contact_form_submissions,
                'signup' => $signup_requests,
                'feedbacks_admin' => $feedbacks_from_admin_panel,
                'feedbacks_quiz' => $feedbacks_from_quiz_system
            );
            return $data;
		}

		else {
		    $data = array(
		        'error' => 'Oops. Something Went Wrong. Please Try Again.'
            );

		    return $data;
        }
	}
}

