<?php

class Feedback_quiz_sys_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        $this->load->library('session');
        parent::__construct();
    }

    public function get_info()
    {

        $sql1 = "SELECT * FROM feedbacks WHERE source='Quiz System'";
        $query1 = $this->db->query($sql1);

        if(isset($query1)){
            return $query1->result_array();
        }

        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );
            return $data;
        }
    }
}

