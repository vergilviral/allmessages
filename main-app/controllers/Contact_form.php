<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_form extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Contact_form_mod');

    }
    public function index()
    {
        if(isset($this->session->user_name)) {
            $info = $this->Contact_form_mod->get_info();

            if (isset($info['error'])) {
                $this->session->set_flashdata('error', $info['error']);
                header("refresh:1,url=" . base_url() . "Contact_form");
            } else {
                $data = array(
                    'info' => $info,
                );

                $message = $this->session->flashdata('message');

                $error = $this->session->flashdata('error');

                $msg = $this->session->flashdata('msg');

                if (isset($message)) {
                    $data['message'] = $message;
                }
                if (isset($error)) {
                    $data['error'] = $error;
                }
                if (isset($msg)) {
                    $data['msg'] = $msg;
                }

                $this->load->view('static/header');
                $this->load->view('pages/contactform', $data);
                $this->load->view('static/footer');
            }
        }
        else{
            $this->session->set_flashdata('error', 'Please login first.');
            header("refresh:1,url=". base_url() ."");

        }
    }

}
