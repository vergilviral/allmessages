<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        
        $this->details = array();
        parent::__construct();
        $this->load->model('home_mod');
//        $this->load->model('Edit_profile_mod');
    }

    public function index()
    {
        if(isset($this->session->user_name)){
            $info = $this->home_mod->get_info();

            $message = $this->session->flashdata('message');

            $msg = $this->session->flashdata('msg');

            $error = $this->session->flashdata('error');

            if (isset($info['error'])) {
                $data = array(
                    'error' => $info['error'],
                );
                $this->load->view('static/header_home', $data);
                $this->load->view('pages/dashboard', $data);
                $this->load->view('static/footer');

            }

            if (!isset($info['error'])) {
                $data2 = array(
                    'info' => $info,
                );
                $this->load->view('static/header', $data2);
                $this->load->view('pages/dashboard', $data2);
                $this->load->view('static/footer');
            }
        }
        else{
            $this->session->set_flashdata('error', 'Please login first.');
            header("refresh:1,url=". base_url() ."");

        }
    }
}
