<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?><div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">All Sign Up For Beta Requests</h4>
                    </div>
                    <div class="card-content table-responsive">

                        <table class="table">
                            <tbody class="centertext">
                            <tr>
                                <th>ID</th>
                                <th>FName</th>
                                <th>LName</th>
                                <th>Email</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Phone</th>
                                <th>Institute</th>
                                <th>Role</th>
                                <th>No. Of Subs</th>
                                <th>No. Of Stus</th>
                                <th>No. Of Quiz</th>
                            </tr>

                            <?php

                            if(!empty($info))
                                foreach ($info as $row)
                                {
                                    echo '<tr>';
                                    echo '<td>' . $row['req_id'] . '</td>';
                                    echo '<td>' . $row['first_name'] . '</td>';
                                    echo '<td>' . $row['last_name'] . '</td>';
                                    echo '<td>' . $row['email'] . '</td>';
                                    echo '<td>' . $row['country'] . '</td>';
                                    echo '<td>' . $row['city'] . '</td>';
                                    echo '<td>' . $row['phone'] . '</td>';
                                    echo '<td>' . $row['institute'] . '</td>';
                                    echo '<td>' . $row['role'] . '</td>';
                                    echo '<td>' . $row['number_of_subjects'] . '</td>';
                                    echo '<td>' . $row['number_of_students'] . '</td>';
                                    echo '<td>' . $row['number_of_quizzes'] . '</td>';
                                    echo '</tr>';
                                }
                            ?>
                            </tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>home">
                                        <button type="button" class="btn btn-primary pull-right">Go Back</button>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
