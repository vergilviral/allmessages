<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="blue">
						<i class="material-icons">note_add</i>
					</div>
					<div class="card-content">
						<p class="category">Total Contact Form Submissions</p>
						<h3 class="title"><?= $info['contact'] ?></h3>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="red">
						<i class="material-icons">account_circle</i>
					</div>
					<div class="card-content">
						<p class="category">Total Signup Requests</p>
                        <h3 class="title"><?= $info['signup'] ?></h3>
                    </div>
				</div>
			</div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">note_add</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Total Feedbacks From Admin Panel</p>
                        <h3 class="title"><?= $info['feedbacks_admin'] ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">account_circle</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Total Feedbacks From Quiz System</p>
                        <h3 class="title"><?= $info['feedbacks_quiz'] ?></h3>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>