<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?><div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">All Feedbacks Received From Quiz System</h4>
                    </div>
                    <div class="card-content table-responsive">

                        <table class="table">
                            <tbody class="centertext">
                            <tr>
                                <th>Feedback ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Feedback</th>
                                <th>Contact Back?</th>
                            </tr>

                            <?php

                            if(!empty($info))
                                foreach ($info as $row)
                                {
                                    echo '<tr>';
                                    echo '<td>' . $row['feedback_id'] . '</td>';
                                    echo '<td>' . $row['username'] . '</td>';
                                    echo '<td>' . $row['email'] . '</td>';
                                    echo '<td>' . $row['feedback'] . '</td>';
                                    echo '<td>' . $row['contact_back'] . '</td>';
                                    echo '</tr>';
                                }
                            ?>
                            </tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>home">
                                        <button type="button" class="btn btn-primary pull-right">Go Back</button>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
