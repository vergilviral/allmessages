<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>VedaQuiz - All Messages</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	<!-- Bootstrap core CSS     -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
	<link href="<?php echo base_url() ?>assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
<!--	<!--  CSS for Demo Purpose, don't include it in your project     -->
<!--	<link href="<?php //echo base_url() ?><!--assets/css/demo.css" rel="stylesheet" />-->
	<!--     Fonts and icons     -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>

    <style>
		::-webkit-input-placeholder { text-align:right; }
		/* mozilla solution */
		input:-moz-placeholder { text-align:right; }
	</style>
<!--	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" type="text/javascript"></script>-->

</head>
 <!---->
<body>
<?php

	if(!isset($_SESSION['user_name'])) {
		header("Location: " . base_url());
		exit;
	}

?>
<div class="wrapper">
	<div class="sidebar" data-color="blue" data-image="<?php echo base_url(); ?>assets/img/sidebar-1.jpg">
        <div class="logo">
			<a href="<?php echo base_url(); ?>home" class="simple-text">
				VedaQuiz
			</a>
		</div>
		<div class="sidebar-wrapper">
			<ul class="nav">
                <li class="active">
                    <a href="<?php echo base_url(); ?>home">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
				<li>
					<a href="<?php echo base_url(); ?>Contact_form">
						<i class="material-icons text-gray">file_download</i>
						<p>From Contact Form</p>
					</a>
				</li>
                <li>
                    <a href="<?php echo base_url(); ?>Signup_req">
                        <i class="material-icons">insert_emoticon</i>
                        <p>Sign Up Requests</p>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" href="#feedback" class="collapsed" aria-expanded="false">
                        <i class="material-icons">question_answer</i>
                        <p> User Feedback
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="feedback" aria-expanded="false" style="height: 147px;">
                        <ul class="nav">
                            <li>
                                <a href="<?php echo base_url(); ?>Feedback_admin">
                                    <i class="material-icons">add</i>
                                    <p>Admin Panel</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>Feedback_quiz_sys">
                                    <i class="material-icons">create</i>
                                    <p>Quiz System</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>AuthCtrl/logout">
                        <i class="material-icons">lock</i>
                        <p>Log Out</p>
                    </a>
                </li>
            </ul>
		</div>
	</div>
	<div class="main-panel">
		<nav class="navbar navbar-transparent navbar-absolute">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
<!--					<a class="navbar-brand" href="#"> Admin Panel </a>-->
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
<!--						<li>-->
<!--							<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">-->
<!--								<i class="material-icons">dashboard</i>-->
<!--								<p class="hidden-lg hidden-md">Dashboard</p>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li class="dropdown">-->
<!--							<a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
<!--								<i class="material-icons">notifications</i>-->
<!--								<span class="notification">5</span>-->
<!--								<p class="hidden-lg hidden-md">Notifications</p>-->
<!--							</a>-->
<!--							<ul class="dropdown-menu">-->
<!--								<li>-->
<!--									<a href="#">Mike John responded to your email</a>-->
<!--								</li>-->
<!--								<li>-->
<!--									<a href="#">You have 5 new tasks</a>-->
<!--								</li>-->
<!--								<li>-->
<!--									<a href="#">You're now friend with Andrew</a>-->
<!--								</li>-->
<!--								<li>-->
<!--									<a href="#">Another Notification</a>-->
<!--								</li>-->
<!--								<li>-->
<!--									<a href="#">Another One</a>-->
<!--								</li>-->
<!--							</ul>-->
<!--						</li>-->
<!--						<li class="dropdown">-->
<!--							<a class="dropdown-toggle" data-toggle="dropdown" href="#">-->
<!--                                <img src="--><?//= base_url() ?><!--uploads/--><?//= $_SESSION['profile_pic'] ?><!--" style="height: 50px; width: 50px; border-radius: 100%;" />-->
<!--							</a>-->
<!--							<ul class="dropdown-menu">-->
<!--								<li>-->
<!--									<a href="--><?php //echo base_url(); ?><!--Edit_profile">Edit Profile</a>-->
<!--								</li>-->
<!--								<li>-->
<!--									<a href="--><?php //echo base_url(); ?><!--AuthCtrl/logout">Log Out</a>-->
<!--								</li>-->
<!--							</ul>-->
<!--						</li>-->
					</ul>
				</div>
			</div>
		</nav>
